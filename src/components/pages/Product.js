import React from 'react';
import { connect } from 'react-redux';
import { fetchProduct } from "../../actions/";

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

class Product extends React.Component {
    componentDidMount() {
        this.props.fetchProduct(this.props.match.params.id);
    }

    render() {
        if (!this.props.fav) {
            return <div>Loading</div>;
        }
        const { product_name, image, price } = this.props.fav;
        return (
            <div>
                <Container maxWidth="lg">
                    <Grid container>
                        <Grid item xs={12} sm={6} md={4} >
                            <img src={image} alt={product_name}/>
                        </Grid>
                        <Grid item xs={12} sm={6} md={8} >
                            <Typography variant="h3" component="h1" color="textPrimary" gutterBottom>
                                {product_name}
                            </Typography>
                            <Typography  variant="subtitle2" component="h6">
                                {price} lei
                        </Typography>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        fav: state.favs[ownProps.match.params.id]
    };
};

export default connect(
    mapStateToProps,
    { fetchProduct }
)(Product);