import React from 'react';
import Header from './Header';
import Footer from './Footer';
import history from '../history';
import { Router, Route, Switch } from 'react-router-dom';
import MyAccount from './account/MyAccount';
import FavoriteProducts from './favorite/FavoriteProduct';
import Homepage from './pages/Homepage';
import Product from './pages/Product';
import Grafic from './grafic';
import EditEmployer from './employers/EditEmployer';
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex"
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(8, 0, 6),
        transition: theme.transitions.create(['width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
}));


const App = () => {
    const classes = useStyles();
    return (
        <Box className={classes.root}>
            <CssBaseline />
            <Router history={history}>
                <Header />
                <main
                    className={classes.content}
                >
                    <Container maxWidth="xl">
                    <Switch>
                        <Route path="/" exact component={Homepage} />
                        <Route path="/autentificare" exact component={MyAccount} />
                        <Route path="/grafic" exact component={Grafic} />

                        <Route path="/favorite" exact component={FavoriteProducts} />
                        <Route path="/product/:product_name/:id" exact component={Product} />
                        <Route path="/employee/:id" exact component={EditEmployer} />
                    </Switch>
                    </Container>
                    <Footer />
                </main>
            </Router>
        </Box>
    );
};

export default App;
