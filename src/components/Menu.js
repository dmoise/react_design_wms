import React from 'react';
import { connect } from 'react-redux';
import { fetchMenu } from "../actions";
import { Link } from 'react-router-dom';
import clsx from "clsx";

import { withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import List from "@material-ui/core/List";
import Collapse from '@material-ui/core/Collapse';
import ListSubheader from "@material-ui/core/ListSubheader";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Tooltip from '@material-ui/core/Tooltip';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Divider from '@material-ui/core/Divider';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import SendIcon from '@material-ui/icons/SendOutlined';
import PanoramaIcon from '@material-ui/icons/PanoramaOutlined';
import ViewQuiltIcon from "@material-ui/icons/ViewQuiltOutlined";

const styles = theme => ({
    list: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
    },
    listItem: {
        textAlign: 'center',
        display: 'block',
    },

    hide: {
        display: 'none',
    },
    vhide: {
        visibility: 'hidden',
    },
    oVisible: {
        overflow: 'visible',
    },
    drawerMinifier: {
        padding: '0 7px',
    },
    drawerExpanded: {
        padding: '0 15px',
    },
    collapseMenu: {
        background: 'linear-gradient(45deg, rgb(255, 255, 255) 0%, rgb(255, 255, 255) 47%, rgb(236, 233, 233) 100%)',
        color: '#777f91',
        borderRadius: `0 0 ${theme.shape.borderRadius}px ${theme.shape.borderRadius}px`,
    },
    nestedListItem: {
        border: '1px solid transparent',
        '&:hover': {
            color: theme.palette.primary.main
        },
        '&.MuiSvgIcon-root': {
            fill: theme.palette.primary.main,
        },
    },
    nested: {
        paddingLeft: '5px',
        '&::before': {
            content: `''`,
            display: 'inline-block',
            verticalAlign: 'middle',
            width: '7px',
            height: '7px',
            border: `1px solid ${theme.palette.text.secondary}`,
            marginRight: '28px',
            borderRadius: '50%'
        },
        '&:hover': {
            color: theme.palette.primary.main,
            backgroundColor: 'transparent',
            '&::before': {
                background: theme.palette.primary.main,
                borderColor: theme.palette.primary.main,
            }
        }
    },
    submenuMinifier: {
        position: 'absolute',
        left: '100%',
        top: '-15px',
        border: `1px solid ${theme.palette.primary.main}`,
        borderRadius: `${theme.shape.borderRadius}px`,
        opacity: 1,
        whiteSpace: 'nowrap',
        paddingRight: '25px',
        zIndex: '-1',
    },
    listItemSelected: {
        border: `1px solid ${theme.palette.primary.main}`,
        borderRightColor: '#fff',
    }
});

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: props.expanded,
            collapse: false,
            openmenu: props.openmenu,
            tooltipOpen: false
        };
    }

    componentDidMount() {
        this.props.fetchMenu();
    }

    handleChange(value) {
        this.handleTooltip(false);
        if (this.state.collapse !== value) {
            this.setState({
                collapse: value
            });
        } else if (this.state.collapse === value) {
            this.setState({
                collapse: false
            });
        }
    }

    handleTooltip = (value) => {
        this.setState({
            tooltipOpen: value
        });
    }

    static getDerivedStateFromProps(props, state) {
        if (props.expanded !== state.expanded) {
            return {
                expanded: props.expanded
            };
        }
        if (props.openmenu !== state.openmenu) {
            return {
                openmenu: props.openmenu,
                collapse: false
            };
        }
        return null;
    }

    renderMenuMinifier() {
        const { classes } = this.props;
        return (
            <>
                <div className="menu-item menu-minify">
                    <ListItem button component={Link} to="/">
                        <Tooltip title="Acasa" placement="right"
                            PopperProps={{
                                popperOptions: {
                                    modifiers: {
                                        offset: {
                                            enabled: true,
                                            offset: '0, 20px',
                                        },
                                    },
                                },
                            }}>
                            <ListItemIcon >
                                <ViewQuiltIcon />
                            </ListItemIcon>
                        </Tooltip>
                    </ListItem>
                </div>
                <Divider />
                {this.props.menu.map(item => {
                    if (item.items.length === 0) {
                        return (
                            <div key={item.id} className="menu-item menu-minify">
                                <ListItem button component={Link} to={`/${item.link}`} className={classes.nestedListItem}>
                                    <Tooltip title={item.title} placement="right"
                                        PopperProps={{
                                            popperOptions: {
                                                modifiers: {
                                                    offset: {
                                                        enabled: true,
                                                        offset: '0, 15px',
                                                    },
                                                },
                                            },
                                        }}>
                                        <ListItemIcon >
                                            <PanoramaIcon fontSize="large" />
                                        </ListItemIcon>
                                    </Tooltip>
                                </ListItem>
                            </div>
                        );
                    } else {
                        return (
                            <div key={item.id} className="menu-item menu-minify">
                                <Tooltip title="Mail" placement="right" className={classes.tooltip} open={this.state.tooltipOpen === `t-${item.id}` ? true : false}>
                                    <ListItem button onClick={() => this.handleChange(`nav-${item.id}`)}  selected={this.state.collapse === `nav-${item.id}` ? true : false}
                                        onMouseEnter={() => { this.handleTooltip(`t-${item.id}`) }}
                                        onMouseLeave={() => { this.handleTooltip(false) }}
                                        className={clsx(classes.nestedListItem, {
                                            [classes.listItemSelected]: this.state.collapse === `nav-${item.id}`,
                                        })}>
                                        <ListItemIcon>
                                            <SendIcon color={this.state.collapse === `nav-${item.id}` ? "primary" : "inherit"} fontSize="small" />
                                        </ListItemIcon>
                                        <Collapse in={this.state.collapse === `nav-${item.id}` ? true : false} timeout="auto" unmountOnExit
                                            className={clsx(classes.collapseMenu, classes.submenuMinifier)}
                                            onMouseEnter={() => { this.handleTooltip(false) }}
                                        >
                                            <List component="div" disablePadding >
                                                {item.items.map(subitem => {
                                                    return (
                                                        <ListItem button key={subitem.id} component={Link} to="/" >
                                                            <ListItemText secondary={subitem.name} className={classes.nested} disableTypography />
                                                        </ListItem>
                                                    );
                                                })}
                                            </List>
                                        </Collapse>
                                    </ListItem>
                                </Tooltip>
                            </div>
                        );
                    }
                })
                }
            </>
        )
    }

    renderMenuExpanded() {
        const { classes } = this.props;
        return (
            <>
                <div className="menu-item menu-expanded">
                    <ListItem button component={Link} to="/">
                        <ListItemIcon >
                            <ViewQuiltIcon fontSize="small" color="disabled" />
                        </ListItemIcon>
                        <ListItemText primary="Acasa" disableTypography />
                    </ListItem>
                </div>
                <ListSubheader component="div" id="nested-list-subheader">
                    Nomenclatoare
                </ListSubheader>
                {this.props.menu.map(item => {
                    if (item.items.length === 0) {
                        return (
                            <div key={item.id} className="menu-item menu-expanded">
                                <ListItem button component={Link} to={`/${item.link}`}>
                                    <ListItemIcon >
                                        <PanoramaIcon fontSize="small" color="disabled" />
                                    </ListItemIcon>
                                    <ListItemText primary={item.title} disableTypography />
                                </ListItem>
                            </div>
                        );
                    } else {
                        return (
                            <div key={item.id} className="menu-item menu-expanded">
                                <ListItem button onClick={() => this.handleChange(`nav-${item.id}`)} value={`nav-${item.id}`}
                                    selected={this.state.collapse === `nav-${item.id}` ? true : false}>
                                    <ListItemIcon>
                                        <SendIcon color={this.state.collapse === `nav-${item.id}` ? "primary" : "disabled"} fontSize="small" />
                                    </ListItemIcon>
                                    <ListItemText secondary={item.title} disableTypography />
                                    {this.state.collapse === `nav-${item.id}` ? <ExpandLess /> : <ExpandMore />}
                                </ListItem>
                                <Collapse in={this.state.collapse === `nav-${item.id}` ? true : false} timeout="auto" unmountOnExit
                                    className={classes.collapseMenu}>
                                    <List component="div" disablePadding >
                                        {item.items.map(subitem => {
                                            return (
                                                <ListItem button key={subitem.id} component={Link} to="/">
                                                    <ListItemText secondary={subitem.name} className={classes.nested} disableTypography />
                                                </ListItem>
                                            );
                                        })}
                                    </List>
                                </Collapse>
                            </div>
                        );
                    }
                })
                }
            </>
        )
    }

    render() {
        const { classes } = this.props;
        return (
            <List
                component="nav"
                aria-labelledby="nested-list-subheader"
                className={clsx(classes.oVisible, {
                    [classes.drawerExpanded]: this.props.expanded,
                    [classes.drawerMinifier]: !this.props.expanded,
                })}
            >
                {this.props.expanded ? this.renderMenuExpanded() : this.renderMenuMinifier()}
            </List>
        );
    }
}

Menu.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        menu: Object.values(state.menu)
    };
};

export default connect(
    mapStateToProps,
    { fetchMenu }
)(withStyles(styles)(Menu));