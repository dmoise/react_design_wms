import React from 'react';
import { Link } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import VisibilityIcon from '@material-ui/icons/Visibility';


const useStyles = makeStyles(theme => ({
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '100%',
    },
    cardContent: {
        flexGrow: 1,
    },
}));

export default function Product(props) {
    const classes = useStyles();
    let {id, product_name, image, price, link_rewrite} = props.product;

    return (
        <Grid item xs={12} sm={6} md={3}>
            <Card className={classes.card}>
                <CardMedia
                    className={`ceva ${classes.cardMedia}`}
                    image={image}
                    title={product_name}
                    alt={product_name}
                />
                <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h6" component="h4">
                        {product_name}
                    </Typography>
                    <Typography gutterBottom variant="subtitle2" component="h6">
                        {price} lei
                        </Typography>
                </CardContent>
                <CardActions align="center">
                    <Link to={`/product/${link_rewrite}/${id}`} style={{ textDecoration: 'none' }}>
                        <Button variant="contained" color="primary">
                            <VisibilityIcon />
                            Vizualizeaza
                                </Button>
                    </Link>
                    {props.children}
                </CardActions>
            </Card>
        </Grid>
    );
}