import React from 'react';
import { connect } from 'react-redux';
import { fetchProducts } from "../../actions/";
import ProductList from '../../components/ProductList';

import DeleteIcon from '@material-ui/icons/Delete';
import { Typography, Grid, Button} from '@material-ui/core';

class FavoriteProducts extends React.Component {
    componentDidMount() {
        this.props.fetchProducts();
    }

    render() {
        return (
            <>
                <Typography variant="h4" align="center" color="textPrimary" style={{ margin: '30px 0' }}>
                    Produse Favorite
                    </Typography>
                <Grid container spacing={2}>
                    <ProductList products={this.props.favs}>
                        <Button variant="contained" color="secondary" >
                            Sterge
                                <DeleteIcon />
                        </Button>
                    </ProductList>
                </Grid>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        favs: Object.values(state.favs)
    };
};

export default connect(
    mapStateToProps,
    { fetchProducts }
)(FavoriteProducts);