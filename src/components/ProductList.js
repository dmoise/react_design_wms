import React from 'react';
import Product from './Product';
import Grid from '@material-ui/core/Grid';

const ProductList = (props) => {
    if (props.products.length === 0) {
        return <div>loading</div>;
    }

    return (
        <Grid container spacing={2}>
            {props.products.map(product => {
                return (
                    <Product key={product.id} product={product}>
                        {props.children}
                    </Product>
                );
            })}
        </Grid>
    )
}

export default ProductList;