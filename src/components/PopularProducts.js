import React from 'react';
import { connect } from 'react-redux';
import { fetchPopulars } from "../actions";

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ProductList from './ProductList';

class PopularProducts extends React.Component {
    componentDidMount() {
        this.props.fetchPopulars();
    }
    render() {
        return (
            <Container maxWidth="lg" >
                <Typography variant="h4" align="center" color="textPrimary" style={{ margin: '30px 0' }}>
                    Produse Populare
                    </Typography>
                <Grid container spacing={2}>
                    <ProductList products={this.props.populars}/>
                </Grid>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        populars: Object.values(state.populars)
    };
};

export default connect(
    mapStateToProps,
    { fetchPopulars }
)(PopularProducts);