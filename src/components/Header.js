import React, { useState } from 'react';
import clsx from "clsx";
import logo from './logo.png';
import Menu from './Menu';
import AccountMenu from './account/AccountMenu';

import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from '@material-ui/core/Tooltip';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Popper from '@material-ui/core/Popper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import MenuIcon from "@material-ui/icons/Menu";
import RightIcon from '@material-ui/icons/ChevronRight';
import LeftIcon from '@material-ui/icons/ChevronLeft';
import SearchIcon from "@material-ui/icons/SearchOutlined";
import ViewQuiltIcon from "@material-ui/icons/ViewQuiltOutlined";


const drawerWidth = 230;
const drawerOpen = 300;
const appWidth = 70;
const drawerMinifierWidth = 70;
const appDrawerOpenMinifier = 140;

const useStyles = makeStyles(theme => ({
    drawer: {
        width: drawerWidth,
        left: appWidth,
        zIndex: '1000',
        background: "linear-gradient(to right, rgb(243, 243, 243) 0%, rgb(245, 245, 245) 67%, rgb(230, 230, 230) 100%)",
    },
    drawerMinifier: {
        width: drawerMinifierWidth,
        overflow: 'visible',
    },
    drawerOpen: {
        width: drawerOpen,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    drawerClose: {
        width: drawerWidth,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    drawerFirst: {
        width: drawerWidth,
    },
    logo: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: '6px 3px 0',
        cursor: 'pointer'
    },
    listToolbar: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
    },
    persistenDrawer: {
        left: '80px',
        zIndex: '1000'
    },
    appBar: {
        left: '0',
        bottom: '0',
        right: 'auto',
        width: '70px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        zIndex: 'auto'
    },
    appBarShift: {
        width: drawerOpen,
        transition: theme.transitions.create(['width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    appBarMinifier: {
        width: appDrawerOpenMinifier,
    },
    toolBar: {
        left: '0',
        bottom: '0',
        right: 'auto',
        width: '70px',
        top: '0',
        display: 'flex',
        position: 'fixed',
        flexDirection: 'column',
        padding: 0,
        height: '100vh',
        backgroundColor: theme.palette.primary.main,
        zIndex: '1024'
    },
    hide: {
        display: 'none',
    },
    vhide: {
        visibility: 'hidden'
    },
    oVisible: {
        overflow: 'visible'
    },
    popover: {
        pointerEvents: 'none',
    },
    spacing1: {
        padding: theme.spacing(1),
    },
    fabMenu: {
        marginTop: '10px',
        marginRight: '10px',
        width: 'auto',
        alignSelf: 'flex-end',
        display: 'flex',
        '&:hover': {
            backgroundColor: 'transparent'
        }
    },
    popperStyle: {
        zIndex: 1201
    }
}));

const Header = () => {
    const classes = useStyles();
    const [openmenu, setOpenmenu] = useState(false);
    const [expandedmenu, setExpandedmenu] = useState(false);
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const [anchorEl, setAnchorEl] = useState(null);

    const handleTooltip = bool => {
        setTooltipOpen(bool);
    }

    const showVersionClick = () => {
        setAnchorEl(!anchorEl);
    };
    function hideVersionClick() {
        setAnchorEl(null);
    }

    function toggleDrawer() {
        setOpenmenu(!openmenu);
    }

    const showVersion = Boolean(anchorEl);
    const id = showVersion ? 'no-transition-popper' : undefined;
    const buttonRef = React.useRef();

    function expandmenu() {
        setExpandedmenu(!expandedmenu);
        handleTooltip(false);
    }

    const sideList = () => (
        <>
            <Drawer
                className={clsx(classes.drawer, {
                    [classes.drawerMinifier]: !expandedmenu,
                })}
                variant="persistent"
                anchor="left"
                open={openmenu}
                classes={{
                    paper: clsx(classes.drawer, {
                        [classes.drawerMinifier]: !expandedmenu,
                    })
                }}
            >
                <Tooltip title={expandedmenu ? 'Micsoreaza meniul' : 'Mareste meniul'} placement="right" disableFocusListener={true} open={tooltipOpen} >
                    <ListItem button onClick={expandmenu} className={clsx({
                        [classes.fabMenu]: expandedmenu
                    })} onMouseEnter={() => { handleTooltip(true) }} onMouseLeave={() => { handleTooltip(false) }}>
                        <ListItemIcon>
                            {!expandedmenu ? <RightIcon fontSize="large" /> : <LeftIcon fontSize="large" />}
                        </ListItemIcon>
                    </ListItem>
                </Tooltip>
                <Menu expanded={expandedmenu} openmenu={openmenu} />
            </Drawer>
        </>
    );

    return (
        <>
            <AppBar position="relative" color="inherit"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: openmenu,
                    [classes.appBarMinifier]: !expandedmenu && openmenu,
                })}
            >
                <Toolbar className={classes.toolBar} disableGutters>
                    <ClickAwayListener onClickAway={hideVersionClick}>
                        <div className={classes.logo} onClick={showVersionClick} ref={buttonRef}>
                            <img src={logo} alt="Logo" style={{ maxWidth: '100%' }} />
                        </div>
                    </ClickAwayListener>
                    <Popper id={id} open={showVersion} anchorEl={() => buttonRef.current} placement="right-start" className={classes.popperStyle}>
                        <Paper>
                            <Typography variant="body1" gutterBottom={false} className={classes.spacing1} >Licenta: SS056R8780F7877W43
                                    <br />Versiune: WMS 14.4.2
                                    <br />Autor: SENIOR SOFTWARE
                                    <br />Revanzator: Astoen Huteck
                        </Typography>
                        </Paper>
                    </Popper>
                    <List className={classes.listToolbar}>
                        <Tooltip title="Meniu" placement="right">
                            <ListItem button className={classes.listItem} onClick={toggleDrawer} selected={openmenu} aria-label="Meniu">
                                <MenuIcon />
                            </ListItem>
                        </Tooltip>
                        <Tooltip title="Cautare" placement="right">
                            <ListItem button className={classes.listItem}>
                                <SearchIcon />
                            </ListItem>
                        </Tooltip>
                        <Tooltip title="Context" placement="right">
                            <ListItem button className={classes.listItem}>
                                <ViewQuiltIcon />
                            </ListItem>
                        </Tooltip>
                    </List>
                    <AccountMenu/>
                </Toolbar>
                {expandedmenu ? sideList() : sideList()}
            </AppBar>
        </>
    );
};

export default Header;

