import React from 'react';
import { connect } from 'react-redux';
import { fetchEmployer, editEmployer } from '../../actions';
//import StreamForm from './StreamForm';

class EditEmployer extends React.Component {
    componentDidMount() {
        this.props.fetchEmployer(this.props.match.params.id);
    }

    onSubmit = (formValues) => {
        this.props.editEmployer(this.props.match.params.id, formValues);
    };

    render() {
        if (!this.props.employers) {
            return <div>Loading...</div>

        }
        return (
            <div>
                <h3>Edit a employer</h3>
                {/* <StreamForm
                    initialValues={_.pick(this.props.stream, 'title', 'description')}
                    onSubmit={this.onSubmit}
                /> */}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return { employers: state.employers[ownProps.match.params.id] };
};

export default connect(mapStateToProps, { fetchEmployer, editEmployer })(EditEmployer);