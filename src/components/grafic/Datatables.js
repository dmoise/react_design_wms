import React from "react";
import { connect } from 'react-redux';
import MUIDataTable from "mui-datatables";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import { fetchEmployers } from "../../actions/";

class Datatables extends React.Component {
  componentDidMount() {
    this.props.fetchEmployers();
  }

  render() {
    const columns = [
      {
        name: "Nume",
        options: {
          filter: false
        }
      },
      {
        name: "Job",
        options: {
          filter: true
        }
      },
      {
        name: "Location",
        options: {
          filter: true
        }
      },
      {
        name: "Age",
        options: {
          filter: false
        }
      },
      {
        name: "Active",
        options: {
          filter: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <FormControlLabel
                label={value ? "Yes" : "No"}
                value={value ? "Yes" : "No"}
                control={
                  <Switch
                    color="primary"
                    checked={value}
                    value={value ? "Yes" : "No"}
                  />
                }
                onChange={(event) => {
                  updateValue(event.target.value === "Yes" ? false : true);
                  console.log(tableMeta.rowData, value);
                }}
              />
            );
          }
        }
      }
    ];

    const options = {
      filter: true,
      filterType: "dropdown"
    };

    return (
      <MUIDataTable
        title={"Employee list"}
        data={this.props.employers}
        columns={columns}
        options={options}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    employers: Object.values(state.employers)
  }
};

export default connect(
  mapStateToProps,
  { fetchEmployers }
)(Datatables);