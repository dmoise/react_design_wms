import React from "react";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/styles';
import Avatar from '@material-ui/core/Avatar';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

const styles = theme => ({
    avatar: {
        color: theme.palette.secondary.main,
        backgroundColor: '#ffffff',
        margin: 'auto',
        position: 'relative',
        '&::before': {
            content: `''`,
            display: 'block',
            width: '100%',
            position: 'absolute',
            top: 0,
            left:0,
            right: 0,
            bottom: 0
        },
    },
    alignBottom: {
        height: '80px',
        width: '100%',
        position: 'relative'
    },
    popperStyle: {
        zIndex: 2201
    },
    // avatarBefore: {
    //     position: 'relative',
    //     '&::before': {
    //         content: `''`,
    //         display: 'block',
    //         width: '100%',
    //         position: 'absolute',
    //         top: 0,
    //         left:0,
    //         right: 0,
    //         bottom: 0
    //     },
    // }
});

class AccountMenu extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            anchorElement: null,
            open: false
        }
    }

    hideOpen = () => this.setState({ ...this.state, open: false });

    handleClick(event) {
        this.setState({ anchorElement: event.target, open: !this.state.open });
    };

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    render() {
        const id = this.state.open ? "account-popper" : undefined;
        const { classes } = this.props;
        return (
            <ClickAwayListener onClickAway={this.hideOpen}>
                <div className={classes.alignBottom}>
                    <div aria-describedby={id}
                        onClick={(event) => this.handleClick(event)}
                        aria-controls={id} aria-haspopup="true" className={classes.avatarBefore}>
                        <Avatar className={classes.avatar} >DE</Avatar>
                    </div>
                    <Popper
                        id={id}
                        open={this.state.open}
                        className={classes.popperStyle}
                        placement="right-end"
                        disablePortal={true}
                        anchorEl={this.state.anchorElement}
                    >
                        <Paper>
                            <ListItem button component={Link} to="/autentificare">
                                <ListItemText primary="Autentificare" />
                            </ListItem>
                            <ListItem button component={Link} to="/contul-meu">
                                <ListItemText primary="Contul meu" />
                            </ListItem>
                        </Paper>
                    </Popper>
                </div>
            </ClickAwayListener>
        );
    }
}

AccountMenu.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AccountMenu);