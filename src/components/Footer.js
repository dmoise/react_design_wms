import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles(theme => ({
  footer: {
    padding: theme.spacing(6),
  },
  marginTop: {
    marginTop: '20px'
  }
}));

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="#">
        Your Website
        </Link>{' '}
      {new Date().getFullYear()}
      {'. Built with '}
      <Link color="inherit" href="#">
        Material-UI.
        </Link>
    </Typography>
  );
}

const Footer = () => {
  const classes = useStyles();
  return (
    <>
    <footer className={classes.footer}>
    <Divider  variant="middle" />
      <Typography variant="h6" align="center" gutterBottom className={classes.marginTop}>
        Footer
      </Typography>
      <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
        Something here to give the footer a purpose!
      </Typography>
      <Copyright />
    </footer>
    </>
  );
}

export default Footer;