import api from '../apis/api';
import history from '../history';
import {
    FETCH_PRODUCTS,
    FETCH_PRODUCT,
    DELETE_PRODUCT,
    FETCH_POPULARS,
    FETCH_EMPLOYERS,
    FETCH_EMPLOYER,
    DELETE_EMPLOYER,
    EDIT_EMPLOYER,
    CREATE_EMPLOYER,
    FETCH_MENU
} from "./types";


export const fetchProducts = () => async dispatch => {
    const response = await api.get('/favorite');
    dispatch({
        type: FETCH_PRODUCTS,
        payload: response.data
    });
};

export const fetchProduct = (id) => async dispatch => {
    const response = await api.get(`/favorite/${id}`);
    dispatch({
        type: FETCH_PRODUCT,
        payload: response.data
    });
};

export const deleteProduct = (id) => async dispatch => {
    await api.delete(`/favorite/${id}`);
    dispatch({
        type: DELETE_PRODUCT,
        payload: id
    });
    history.push('/');
};


export const fetchPopulars = () => async dispatch => {
    const response = await api.get('/populars');
    dispatch({
        type: FETCH_POPULARS,
        payload: response.data
    });
};

export const fetchEmployers = () => async dispatch => {
    const response = await api.get('/employee');
    dispatch({
        type: FETCH_EMPLOYERS,
        payload: response.data
    });
};

export const fetchEmployer = (id) => async dispatch => {
    const response = await api.get(`/employee/${id}`);
    dispatch({
        type: FETCH_EMPLOYER,
        payload: response.data
    });
};
export const editEmployer = (id, formValues) => async dispatch => {
    const response = await api.patch(`/employee/${id}`, formValues);
    dispatch({
        type: EDIT_EMPLOYER,
        payload: response.data
    });
    history.push('/grafic');
};

export const deleteEmployer = (id) => async dispatch => {
    await api.delete(`/employee/${id}`);
    dispatch({
        type: DELETE_EMPLOYER,
        payload: id
    });
    history.push('/grafic');
};

export const createEmployer = (formValues) => async (dispatch, getState) => {
    const response = await api.post('/employee', formValues);
    dispatch({
        type: CREATE_EMPLOYER,
        payload: response.data
    });

    // Get the user back to the root
    history.push('/grafic');
};



export const fetchMenu = () => async dispatch => {
    const response = await api.get('/menu');
    dispatch({
        type: FETCH_MENU,
        payload: response.data
    });
};
