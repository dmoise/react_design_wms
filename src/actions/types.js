// Produse Favorite
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const FETCH_PRODUCT = 'FETCH_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';


// Produse populare
export const FETCH_POPULARS = 'FETCH_POPULARS';

// Employers
export const FETCH_EMPLOYERS = 'FETCH_EMPLOYERS';
export const FETCH_EMPLOYER = 'FETCH_EMPLOYER';
export const DELETE_EMPLOYER = 'DELETE_EMPLOYER';
export const EDIT_EMPLOYER = 'EDIT_EMPLOYER';
export const CREATE_EMPLOYER = 'CREATE_EMPLOYER';

export const FETCH_MENU = 'FETCH_MENU';