import {combineReducers} from 'redux';
import favReducer from './favReducer';
import popularReducer from './popularReducer';
import employersReducer from './employersReducer';
import menuReducer from './menuReducer';

export default combineReducers({
    favs: favReducer,
    populars:  popularReducer,
    employers: employersReducer,
    menu: menuReducer
});