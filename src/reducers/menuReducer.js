import _ from 'lodash';
import {
    FETCH_MENU
} from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_MENU:
            return {...state, ..._.mapKeys(action.payload, 'id')};
        default:
            return state;
    }
}