import _ from 'lodash';
import {
    FETCH_EMPLOYERS,
    FETCH_EMPLOYER,
    DELETE_EMPLOYER,
    EDIT_EMPLOYER,
    CREATE_EMPLOYER
} from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_EMPLOYERS:
            return {...state, ..._.mapKeys(action.payload, 'id')};
        case FETCH_EMPLOYER:
            return {...state, [action.payload.id]: action.payload};
        case CREATE_EMPLOYER:
            return {...state, [action.payload.id]: action.payload};
        case EDIT_EMPLOYER:
            return { ...state, [action.payload.id]: action.payload };
        case DELETE_EMPLOYER:
            return _.omit(state, action.payload);
        default:
            return state;
    }
}