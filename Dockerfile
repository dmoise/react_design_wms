FROM node:12.6.0-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

RUN npm install create-react-app -g
RUN npm install react-scripts -g